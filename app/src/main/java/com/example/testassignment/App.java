package com.example.testassignment;

import android.app.Application;

import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.strategy.SocketInternetObservingStrategy;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by oniyide on 19/04/2018.
 */

public class App extends Application {
    private static App instance;

    private Observable<Boolean> connectionStatus;

    @Override
    public void onCreate() {
        super.onCreate();
        App.instance = this;
        Timber.plant(new Timber.DebugTree());
        connectionStatus = ReactiveNetwork.observeInternetConnectivity(new SocketInternetObservingStrategy(), "www.google.com")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());


    }

    public Observable<Boolean> getConnectionStatus() {
        return connectionStatus;
    }

    public static App getInstance() {
        return instance;
    }
}
