package com.example.testassignment.adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testassignment.Constants;
import com.example.testassignment.R;
import com.example.testassignment.models.Post;
import com.example.testassignment.viewModel.MainViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by oniyideinc on 4/22/18.
 */

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.Holder> {

    private MainViewModel mainViewModel;
    private ArrayList<Post> posts = new ArrayList<>();

    public PostsAdapter(MainViewModel mainViewModel) {
        this.mainViewModel = mainViewModel;
        mainViewModel.getPostsAsObservable().subscribe(this::update);
    }

    private void update(ArrayList<Post> posts) {
        this.posts.clear();
        this.posts.addAll(posts);
        notifyDataSetChanged();

    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Post post = posts.get(position);
        holder.post = post;
        holder.score.setText(String.format("+%s", post.data.score));
        holder.title.setText(post.data.title);
        holder.subreddit.setText(post.data.subreddit);

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_post_title)
        TextView title;
        @BindView(R.id.item_post_score)
        TextView score;
        @BindView(R.id.item_post_subreddit)
        TextView subreddit;
        Post post;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_post_frame)
        public void displayInWeb() {

            if (!mainViewModel.isConnected()) {
                Toast.makeText(itemView.getContext(), R.string.unable_to_connect, Toast.LENGTH_SHORT).show();
                return;
            }

            if (TextUtils.isEmpty(post.data.permalink)) {
                Toast.makeText(itemView.getContext(), R.string.invalid_url, Toast.LENGTH_SHORT).show();
                return;
            }

            openInCustomTab();
        }

        private void launchUrl(){
            // Allow device handle the Intent
            Intent intent = new Intent(Intent.ACTION_VIEW,  Uri.parse(String.format("%s%s", Constants.BASE_URL, post.data.permalink)));
            itemView.getContext().startActivity(intent);
        }

        private void openInCustomTab(){
            // chrome custom in app browser
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.setToolbarColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary));
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(itemView.getContext(), Uri.parse(String.format("%s%s", Constants.BASE_URL, post.data.permalink)));
        }
    }
}
