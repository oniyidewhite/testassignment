package com.example.testassignment.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.example.testassignment.R;

import timber.log.Timber;

/**
 * Created by oniyideinc on 4/22/18.
 */

public class RefreshHeaderViewImpl extends RefreshHeaderView {

    public RefreshHeaderViewImpl(@NonNull Context context) {
        super(context);
        setLayout();
    }

    public RefreshHeaderViewImpl(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setLayout();
    }

    public RefreshHeaderViewImpl(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setLayout();
    }

    protected void setLayout() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_refresh_header, this, false);
        addView(view);
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        Timber.i("onRefresh");
    }

    @Override
    public void onStart(boolean b, int i, int i1) {
        super.onStart(b, i, i1);
        Timber.i("onRefresh");
    }

}
