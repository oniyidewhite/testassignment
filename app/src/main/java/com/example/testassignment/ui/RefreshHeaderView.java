package com.example.testassignment.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.aspsine.irecyclerview.RefreshTrigger;

/**
 * Created by oniyideinc on 4/22/18.
 */

public abstract class RefreshHeaderView extends FrameLayout implements RefreshTrigger {
    public RefreshHeaderView(@NonNull Context context) {
        super(context);
    }

    public RefreshHeaderView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RefreshHeaderView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onStart(boolean b, int i, int i1) {

    }

    @Override
    public void onMove(boolean b, boolean b1, int i) {

    }

    @Override
    public void onRefresh() {
        // refresh data
    }

    @Override
    public void onRelease() {

    }

    @Override
    public void onComplete() {
        // done
    }

    @Override
    public void onReset() {

    }
}
