package com.example.testassignment.interfaces;

import com.example.testassignment.Constants;
import com.example.testassignment.models.Post;
import com.example.testassignment.models.PostResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by oniyideinc on 4/22/18.
 */

public interface Api {
    @GET(Constants.POSTS)
    Observable<PostResponse> fetch(@Query("after") String page);
}
