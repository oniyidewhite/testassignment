package com.example.testassignment.viewModel;

import com.example.testassignment.http.ApiManager;
import com.example.testassignment.models.Post;
import com.example.testassignment.models.PostResponse;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by oniyideinc on 4/22/18.
 */

public class MainViewModel {

    private static final String TAG = MainViewModel.class.getSimpleName();

    private static MainViewModel instance;
    private ArrayList<Post> posts = new ArrayList<>();
    private Observable<ArrayList<Post>> observable;

    private String nextPage;

    private ApiManager manager;
    private Disposable disposable;

    boolean isLoading;
    boolean reload;

    private boolean hasInternet;

    private ObservableEmitter<ArrayList<Post>> emitter;

    private String error;
    private ObservableEmitter<String> errorEmitter;
    private Observable<String> errorObservable;

    private MainViewModel() {
        MainViewModel.instance = this;

        observable = Observable.create(this::setEmitter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        errorObservable = Observable.create(this::setErrorEmitter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        manager = ApiManager.getInstance();

    }

    private void setEmitter(ObservableEmitter<ArrayList<Post>> emitter) {
        this.emitter = emitter;
    }

    private void setErrorEmitter(ObservableEmitter<String> emitter) {
        this.errorEmitter = emitter;
    }

    public void init() {
        if (posts.isEmpty()) {
            reload();
        } else {
            emitter.onNext(posts);
        }
    }

    public void reload() {
        if (!isLoading && !reload) {
            isLoading = true;
            reload = true;
            nextPage = null;
            posts.clear();
            makeRequest();
        }
    }

    public boolean isLoading(){
        return isLoading;
    }

    public void loadNextPage() {
        if (reload)
            return;

        if (!isLoading) {
            isLoading = true;
            makeRequest();
        }

    }

    public boolean isEmpty() {
        return posts.isEmpty();
    }

    private void makeRequest() {
        manager.fetch(nextPage).subscribe(new Observer<PostResponse>() {

            private Disposable disposable;

            @Override
            public void onSubscribe(Disposable d) {
                // do nothing
                disposable = d;
            }

            @Override
            public void onNext(PostResponse post) {
                Timber.i("%s: onNext with count %s", TAG, post.data.children.size());
                nextPage = post.data.after;

                posts.addAll(post.data.children);
                emitter.onNext(posts);
                errorEmitter.onNext("");
                update();

                //networkError = null;
            }

            @Override
            public void onError(Throwable e) {
                Timber.i(e);

                errorEmitter.onNext(e.getLocalizedMessage());
                update();

                //networkError = e.getLocalizedMessage();
            }

            private void update() {
                isLoading = false;
                reload = false;
                if (!disposable.isDisposed())
                    disposable.dispose();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void setHasInternet(boolean hasInternet) {
        this.hasInternet = hasInternet;
        if (hasInternet && posts.isEmpty()) {
            init();
        }
    }

    public boolean isConnected() {
        return hasInternet;
    }

    public Observable<ArrayList<Post>> getPostsAsObservable() {
        return observable;
    }

    public Observable<String> getErrorAsObservable() {
        return errorObservable;
    }

    public static MainViewModel getInstance() {
        return instance != null ? instance : new MainViewModel();
    }
}
