package com.example.testassignment.http;

import com.example.testassignment.Constants;
import com.example.testassignment.interfaces.Api;
import com.example.testassignment.models.PostResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by oniyideinc on 4/22/18.
 */

public class ApiManager {
    private static ApiManager instance;
    private Api webservice;

    private ApiManager() {
        ApiManager.instance = this;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        webservice = retrofit.create(Api.class);
    }

    public static ApiManager getInstance() {
        return instance != null ? instance : new ApiManager();
    }


    public Observable<PostResponse> fetch(String page) {
        return webservice.fetch(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

}
