package com.example.testassignment.activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.aspsine.irecyclerview.IRecyclerView;
import com.example.testassignment.App;
import com.example.testassignment.R;
import com.example.testassignment.adapter.PostsAdapter;
import com.example.testassignment.http.ApiManager;
import com.example.testassignment.viewModel.MainViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler_items)
    IRecyclerView mRecyclerView;

    @BindView(R.id.activity_main_error)
    View error;

    private MainViewModel mainViewModel;
    private Disposable disposable, connectionDisposable;
    private App app = App.getInstance();

    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        mainViewModel = MainViewModel.getInstance();
        register();

        ApiManager.getInstance().fetch(null);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setIAdapter(new PostsAdapter(mainViewModel));


        mainViewModel.init();


    }


    @Override
    protected void onStart() {
        super.onStart();

        disposable = mainViewModel.getErrorAsObservable().subscribe(this::update);

        connectionDisposable = app.getConnectionStatus().subscribe(aBoolean -> {

            // check if empty
            // if empty show error msg
            // else show internet stat
            mainViewModel.setHasInternet(aBoolean);
            if (mainViewModel.isEmpty() && !aBoolean) {

                //show main error
                error.setVisibility(View.VISIBLE);
                displayInternetError();

            } else if (!aBoolean) {
                // show snackbar
                error.setVisibility(View.GONE);
                displayInternetError();
            } else {
                if (snackbar != null)
                    snackbar.dismiss();
                error.setVisibility(View.GONE);
            }


        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        //bugfix
        if(!mainViewModel.isLoading()){
            mRecyclerView.setRefreshing(false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();

        if (connectionDisposable != null && !connectionDisposable.isDisposed())
            connectionDisposable.dispose();
    }

    private void update(String msg) {
        mRecyclerView.setRefreshing(false);
        if (!TextUtils.isEmpty(msg))
            Toast.makeText(App.getInstance(), msg, Toast.LENGTH_SHORT).show();
    }

    private void register() {
        mRecyclerView.setOnLoadMoreListener(() -> {
            mainViewModel.loadNextPage();
        });

        mRecyclerView.setOnRefreshListener(() -> {

            if (!mainViewModel.isConnected()){
                mRecyclerView.setRefreshing(false);
                return;
            }

            mainViewModel.reload();
        });
    }


    private void displayInternetError() {
        snackbar = Snackbar.make(mRecyclerView, getString(R.string.unable_to_connect), Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

}
