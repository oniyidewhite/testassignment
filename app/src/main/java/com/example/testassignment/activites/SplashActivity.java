package com.example.testassignment.activites;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.testassignment.R;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SplashActivity extends AppCompatActivity {

    private Disposable mDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

    }

    @Override
    protected void onStart() {
        super.onStart();

        Observable<Long> observable = Observable.interval(3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        mDisposable = observable.subscribe(this::loadNextActivity);
    }

    @Override
    protected void onStop() {
        super.onStop();
        dispose();
    }

    private void loadNextActivity(Long l) {
        dispose();
        startActivity(MainActivity.getStartIntent(this));
        finish();
    }

    private void dispose() {
        if (!mDisposable.isDisposed()) {
            mDisposable.dispose();
        }
    }
}
