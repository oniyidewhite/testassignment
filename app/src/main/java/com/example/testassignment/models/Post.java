package com.example.testassignment.models;

/**
 * Created by oniyideinc on 4/22/18.
 */

public class Post {

    public Data data;

    public static class Data {

        public String subreddit;
        public int score;
        public String title;
        public String permalink;
    }
}
