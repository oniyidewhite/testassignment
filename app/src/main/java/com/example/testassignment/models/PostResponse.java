package com.example.testassignment.models;

import java.util.List;

/**
 * Created by oniyideinc on 4/22/18.
 */

public class PostResponse {

    public ResponseData data;

    public static class ResponseData {
        public String after;
        public List<Post> children;
    }
}
